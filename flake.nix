{
  description = "Devin's UNIX Configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, home-manager, ... }: let
    stateVersion = "23.11";
    system = "x86_64-linux";
    username = "solarinas";

    pkgs = import nixpkgs {
      inherit system;
      config.allowUnfree = true;
    };

    homeDirPrefix = if pkgs.stdenv.hostPlatform.isDarwin then "/Users" else "/home";
    homeDirectory = "${homeDirPrefix}/${username}";

    home = (import ./home.nix {
      inherit homeDirectory pkgs stateVersion system username;
    });
  in {
    homeConfigurations.${username} = home-manager.lib.homeManagerConfiguration {
      inherit pkgs;
      modules = [
        home
      ];
    };
  };
}
