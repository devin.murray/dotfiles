{
  programs.zsh = {
    enable = true;
    enableCompletion = true;
    zplug = {
      enable = true;
      plugins = [
        { name = "zsh-users/zsh-autosuggestions"; }
        { name = "zsh-users/zsh-syntax-highlighting"; }
        { name = "zsh-users/zsh-history-substring-search"; }
        { name = "jeffreytse/zsh-vi-mode"; }
      ];
    };
  };

programs.git = {
    enable = true;
    userEmail = "devin.murray@devinops.ca";
    userName = "Devin Murray";
    extraConfig = {
      init.defaultBranch = "main";
      gitlab.user = "devin.murray";
    };
    signing = {
      key = "B3800BA80F9E49B2";
      signByDefault = true;
    };
  };

  programs.gpg = {
    enable = true;
    scdaemonSettings = {
      disable-ccid = true;
    };
  };

  programs.starship = {
    enable = true;
  };

  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
    nix-direnv.enable = true;
  };
}
