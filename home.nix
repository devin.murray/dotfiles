{ homeDirectory, pkgs, stateVersion, system, username }:{

  targets.genericLinux.enable = true;
  programs.home-manager.enable = true;

  xdg = {
    enable = true;
    mime.enable = true;
    systemDirs.data = [ "${homeDirectory}/.nix-profile/share/applications" ];
  };

  imports = [
    ./nvim
    ./emacs
    ./shell
  ];

  home = {
    inherit homeDirectory stateVersion username;
    packages = with pkgs; [
      # Emacs dependencies
      pinentry-qt
    ];
  };

  programs.vscode = {
    enable = true;
    enableUpdateCheck = true;
    enableExtensionUpdateCheck = true;
    mutableExtensionsDir = true;
    extensions = with pkgs.vscode-extensions; [
      asvetliakov.vscode-neovim
      ms-vscode-remote.remote-containers
      mkhl.direnv
    ];
  };

  services = {
    gpg-agent = {
      enable = true;
      enableSshSupport = true;
      enableScDaemon = true;
      enableZshIntegration = true;
      grabKeyboardAndMouse = true;
      pinentryFlavor = "qt";
      sshKeys = [
        "B3800BA80F9E49B2"
      ];
      defaultCacheTtl = 60;
      maxCacheTtl = 120;
      defaultCacheTtlSsh = 60;
      maxCacheTtlSsh = 120;
    };
    syncthing = {
      enable = true;
      tray.enable = true;
    };
  };
}
