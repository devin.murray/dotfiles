{ pkgs, ... }:{

  programs.emacs = {
    enable = true;
    package = pkgs.emacs-gtk;
  };

  home.packages = with pkgs; [
    # Extension dependencies
    ripgrep
    # Vterm build dependencies
    libvterm
    cmake
    libtool
  ];

  home.file.".emacs.d" = {
    source = builtins.fetchGit {
      url = "https://gitlab.com/devin.murray/solmacs.git";
      rev = "1d3af7716ca6ee3e353b8f49399241d7753dbba7";
    };
    recursive = true;
  };
}
