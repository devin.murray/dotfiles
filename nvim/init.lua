-- Vim Options
vim.o.clipboard = "unnamedplus" -- Sync System clipboard
vim.o.laststatus = 0 -- Disable Statusline
vim.wo.relativenumber = true

vim.bo.tabstop = 4
vim.bo.shiftwidth = 4
vim.o.smarttab = true
vim.bo.expandtab = true
vim.wo.wrap = true
vim.bo.textwidth = 79
vim.bo.formatoptions = 'qrn1'

vim.o.undofile = true
vim.o.undodir = vim.fn.expand('~/.config/nvim/undo')

vim.g.mapleader = "," -- Set Leader key

-- Bootstrap Lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Load Neovim Plugins
require("lazy").setup({
  "preservim/nerdcommenter",
  "tpope/vim-surround",
  "joshdick/onedark.vim"
  })

-- Set colorscheme
vim.cmd('colorscheme onedark')
vim.cmd('highlight Normal ctermbg=NONE') -- Make terminal background transparent
