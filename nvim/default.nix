{
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    viAlias = true;
  };

  xdg.configFile = {
    "nvim/init.lua".source = ./init.lua;
  };
}
